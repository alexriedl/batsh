#!/usr/bin/env bash

VERSION="0.0.1"
REPO="alexriedl/batsh"

docker build -t "${REPO}" .
docker tag "${REPO}" "${REPO}:${VERSION}"
docker push "${REPO}:${VERSION}"
