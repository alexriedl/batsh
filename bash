#!/usr/bin/env bash

shopt -s nullglob

list=($@)
for input_file in "${list[@]}";
do
  output_file="${input_file%.*}.sh"
  echo "Processing ${input_file} > ${output_file}"
  docker run --rm -v "$(pwd):/x" -w /x alexriedl/batsh batsh bash "${input_file}" > "${output_file}"
  chmod +x "${output_file}"
done
