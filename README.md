# Batsh #

Batsh Repository: https://github.com/BYVoid/Batsh

Batsh is a simple programming language that compiles to Bash and Windows Batch. It enables you to write your script once runs on all platforms without any additional dependency.

Both Bash and Batch are messy to read and tricky to write due to historical reasons. You have to spend a lot of time learning either of them and write platform-dependent code for each operating system. I have wasted lots of time in my life struggling with bizarre syntaxes and unreasonable behaviors of them, and do not want to waste any more.

If you happen to be a maintainer of a cross-platform tool which relies on Bash on Linux/Mac and Batch on Windows as "glue code", and found it painful to "synchronize" between them, you would definitely like to try Batsh.

# Example Usage #
Example script provided for easily converting all files in a directory to bash. If you have the following folder structure:

```
- fileone.batsh
- filetwo.batsh
```

you can run `./bash *.batsh` to convert all scripts to bash. The result would be: 
```
- fileone.batsh
- fileone.sh
- filetwo.batsh
- filetwo.sh
```
